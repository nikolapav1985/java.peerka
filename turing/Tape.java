package turing;
import turing.Cell;
/**
*
* Tape class
*
* it is used to represent tape (as doubly linked list)
*
* ----- methods -----
*
* getCurrentCell() get pointer to current cell DONE
* getContent() get char from current cell DONE
* setContent(char ch) set content of current cell DONE
* moveLeft() move left one cell, if leftmost add one more cell DONE
* moveRight() move right one cell, if rightmost add one more cell DONE
* getTapeContents() get content as string, discard leading blanks DONE
*
* ----- compile -----
*
* javac turing/TestTape.java
*
* ----- run -----
*
* java turing.TestTape
*
*/
class Tape{
    Cell head;
    Cell tail;
    Cell current;
    public Tape(){
        this.head=null;
        this.tail=null;
        this.current=null;
    }
    public void moveLeft(){ // move left one cell
        Cell c;
        if(current==null){ // tape is empty
            c=new Cell();
            current=c;
            head=c;
            tail=c;
        }else if(current.prev==null){ // no previous cell
            c=new Cell();
            current.prev=c;
            c.next=current;
            current=current.prev;
            head=current;
        }else{ // there is previous cell
            current=current.prev;
        }
    }
    public void moveRight(){ // move right one cell
        Cell c;
        if(current==null){ // tape is empty
            c=new Cell();
            current=c;
            head=c;
            tail=c;
        }else if(current.next==null){ // no next cell
            c=new Cell();
            current.next=c;
            c.prev=current;
            current=current.next;
            tail=current;
        }else{ // there is next cell
            current=current.next;
        }
    }
    public String getTapeContents(){ // read content from tape, head to tail
        String str="";
        Cell move=head;
        for(;move!=null;move=move.next){
            str+=move.content;
        }
        return str;
    }
    public void setContent(char ch){ // set content current cell
        Cell c;
        if(current==null){
            c=new Cell();
            c.content=ch;
            current=c;
            head=c;
            tail=c;
        } else {
            current.content=ch;
        }
    }
    public char getContent(){ // get content current cell
        return current.content;
    }
    public Cell getCurrentCell(){ // return pointer to current cell
        return current;
    }
}
